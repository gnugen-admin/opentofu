spec:
  inputs:
    # Job and Stage name
    as:
      default: 'custom'
      description: 'Defines the name of this job.'
    stage:
      default: 'test'
      description: 'Defines the stage that this job will belong to.'

    # Versions
    # This version is only required, because we cannot access the context of the component,
    # see https://gitlab.com/gitlab-org/gitlab/-/issues/438275
    version:
      default: 'latest'
      description: 'Version of this component. Has to be the same as the one in the component include entry.'

    base_os:
      default: 'alpine'
      options:
        - 'alpine'
        - 'debian'
        - '$GITLAB_OPENTOFU_BASE_IMAGE_OS'
      description: 'Base OS of GitLab OpenTofu image.'

    opentofu_version:
      default: '1.8.4'
      options:
        - '1.8.4'
        - '1.8.3'
        - '1.8.2'
        - '1.8.1'
        - '1.8.0'
        - '1.7.3'
        - '1.7.2'
        - '1.7.1'
        - '1.7.0'
        - '1.7.0-alpha1'
        - '1.6.2'
        - '1.6.1'
        - '1.6.0'
        - '$OPENTOFU_VERSION'
      description: 'OpenTofu version that should be used.'

    # Images
    image_registry_base:
      default: '$CI_TEMPLATE_REGISTRY_HOST/components/opentofu'
      description: 'Host URI to the job images. Will be combined with `image_name` to construct the actual image URI.'
    # FIXME: not yet possible because of https://gitlab.com/gitlab-org/gitlab/-/issues/438722
    # gitlab_opentofu_image:
    #   # FIXME: This should reference the component tag that is used.
    #   #        Currently, blocked by https://gitlab.com/gitlab-org/gitlab/-/issues/438275
    #   # default: '$CI_TEMPLATE_REGISTRY_HOST/components/opentofu/gitlab-opentofu:$[[ inputs.opentofu_version ]]'
    #   default: '$CI_TEMPLATE_REGISTRY_HOST/components/opentofu/gitlab-opentofu:$[[ inputs.version ]]-opentofu$[[ inputs.opentofu_version ]]'
    #   description: 'Tag of the gitlab-opentofu image.'

    image_name:
      default: 'gitlab-opentofu'
      description: 'Image name for the job images. Hosted under `image_registry_base`.'

    image_digest:
      default: ''
      # FIXME: we cannot use regex yet because of a bug that rejects
      # empty strings from ever being checked against the regex.
      # see https://gitlab.com/gitlab-org/gitlab/-/issues/477707
      # regex: '^(@sha256:[a-z0-9]{64})?$'
      description: 'Image digest of the image you want to use. The format must be `@<image_digest>`, e.g. `@sha256:abc..`, see regex of this input. Please consult the release page at https://gitlab.com/components/opentofu/-/releases to obtain the image digests.'

    # Configuration
    root_dir:
      default: ${CI_PROJECT_DIR}
      description: 'Root directory for the OpenTofu project.'

    command:
      description: 'The gitlab-tofu command to run.'

---

'$[[ inputs.as ]]':
  stage: $[[ inputs.stage ]]
  needs: []
  cache:
    key: "$__CACHE_KEY_HACK"
    paths:
      - $TF_ROOT/.terraform/
  variables:
    # FIXME: work around to make slashes work in `cache:key`. see https://gitlab.com/gitlab-org/gitlab/-/issues/439898
    __CACHE_KEY_HACK: "$[[ inputs.root_dir ]]"
    TF_ROOT: $[[ inputs.root_dir ]]
  image:
    name: '$[[ inputs.image_registry_base ]]/$[[ inputs.image_name ]]:$[[ inputs.version ]]-opentofu$[[ inputs.opentofu_version ]]-$[[ inputs.base_os ]]$[[ inputs.image_digest ]]'
  script:
    - gitlab-tofu $[[ inputs.command ]]

