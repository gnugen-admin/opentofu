## 0.38.0 (2024-11-02)

### Fixed (2 changes)

- [Fix job dependency in full-pipeline when using job prefix](https://gitlab.com/components/opentofu/-/commit/ddf4404f2b0c6df799c40c15eabb08aeca623e90)
- [Fix component version regex in renovate config](https://gitlab.com/components/opentofu/-/commit/879b197004e5d22f57e7f9cc590ba8165f7f2d96)

## 0.37.0-rc7 (2024-10-31)

### Added (4 changes)

- [Support var_file input in all templates](https://gitlab.com/components/opentofu/-/commit/e5efc717c1343000432f0b076933243dc8726770) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/160))
- [Document renovate usage for version input](https://gitlab.com/components/opentofu/-/commit/4827eb3c8f0a738bed6095f71a080ff4a6888f6b)
- [Support renovate custom manager for component version annotation](https://gitlab.com/components/opentofu/-/commit/106ada49cf28a34dcda1e6456003fc05b4ad9ac2)
- [Support unquoted opentofu_version input variables in renovate config](https://gitlab.com/components/opentofu/-/commit/47764fbd6179fb986f4bc0132bacba2208b24757)

### Fixed (1 change)

- [Fix renovate custom manager currentValue delimiters](https://gitlab.com/components/opentofu/-/commit/aadc8ab2c77a0effabfe4143847f52cc2af82a19)

### Development (4 changes)

- [Fix last stable version finding in release script](https://gitlab.com/components/opentofu/-/commit/f5e24ee1c793a9393e5a31b7afb5f8c42f49ddd5)
- [Use sh in README generation script instead of bash](https://gitlab.com/components/opentofu/-/commit/50b559706ced918875ecd186f440f64ddd8835d1)
- [Rename release notes generation script](https://gitlab.com/components/opentofu/-/commit/744a6c388e3cdbe1a97c5038d0131b34852feffc)
- [Outsource README generation script from Makefile into script](https://gitlab.com/components/opentofu/-/commit/b6c567adb188ec34f4a77f234bebbe52493bf3a4)

## 0.37.0-rc6 (2024-10-31)

### Development (4 changes)

- [Fix last stable version finding in release script](https://gitlab.com/components/opentofu/-/commit/f5e24ee1c793a9393e5a31b7afb5f8c42f49ddd5)
- [Use sh in README generation script instead of bash](https://gitlab.com/components/opentofu/-/commit/50b559706ced918875ecd186f440f64ddd8835d1)
- [Rename release notes generation script](https://gitlab.com/components/opentofu/-/commit/744a6c388e3cdbe1a97c5038d0131b34852feffc)
- [Outsource README generation script from Makefile into script](https://gitlab.com/components/opentofu/-/commit/b6c567adb188ec34f4a77f234bebbe52493bf3a4)

## 0.37.0-rc5 (2024-10-31)

No changes.

## 0.37.0-rc4 (2024-10-31)

No changes.

## 0.37.0-rc3 (2024-10-31)

No changes.

## 0.37.0-rc2 (2024-10-31)

No changes.

## 0.37.0-rc1 (2024-10-31)

No changes.

## 0.36.0 (2024-10-31)

No changes.

## 0.35.0-rc6 (2024-10-30)

### Fixed (1 change)

- [Only sign images on GitLab.com](https://gitlab.com/components/opentofu/-/commit/bafd24de55b17039204e9dbf1a6c9cdbb718a009) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/155))

### feature (2 changes)

- [Add support for job name prefix in full-pipeline](https://gitlab.com/components/opentofu/-/commit/72675b768dc3ddf81b0d665e0b5eadbebe10388e) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/153))
- [Support specifying image digests](https://gitlab.com/components/opentofu/-/commit/e6460418e6f1846f16474f6cadab2b935be556e7) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/152))

## 0.35.0-rc5 (2024-10-30)

### Fixed (1 change)

- [Only sign images on GitLab.com](https://gitlab.com/components/opentofu/-/commit/bafd24de55b17039204e9dbf1a6c9cdbb718a009) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/155))

### feature (2 changes)

- [Add support for job name prefix in full-pipeline](https://gitlab.com/components/opentofu/-/commit/72675b768dc3ddf81b0d665e0b5eadbebe10388e) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/153))
- [Support specifying image digests](https://gitlab.com/components/opentofu/-/commit/e6460418e6f1846f16474f6cadab2b935be556e7) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/152))

## 0.35.0-rc4 (2024-10-30)

### Fixed (1 change)

- [Only sign images on GitLab.com](https://gitlab.com/components/opentofu/-/commit/bafd24de55b17039204e9dbf1a6c9cdbb718a009) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/155))

### feature (2 changes)

- [Add support for job name prefix in full-pipeline](https://gitlab.com/components/opentofu/-/commit/72675b768dc3ddf81b0d665e0b5eadbebe10388e) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/153))
- [Support specifying image digests](https://gitlab.com/components/opentofu/-/commit/e6460418e6f1846f16474f6cadab2b935be556e7) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/152))

## 0.35.0-rc3 (2024-10-30)

### Fixed (1 change)

- [Only sign images on GitLab.com](https://gitlab.com/components/opentofu/-/commit/bafd24de55b17039204e9dbf1a6c9cdbb718a009) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/155))

### feature (2 changes)

- [Add support for job name prefix in full-pipeline](https://gitlab.com/components/opentofu/-/commit/72675b768dc3ddf81b0d665e0b5eadbebe10388e) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/153))
- [Support specifying image digests](https://gitlab.com/components/opentofu/-/commit/e6460418e6f1846f16474f6cadab2b935be556e7) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/152))

## 0.35.0-rc2 (2024-10-30)

### Fixed (1 change)

- [Only sign images on GitLab.com](https://gitlab.com/components/opentofu/-/commit/bafd24de55b17039204e9dbf1a6c9cdbb718a009) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/155))

### feature (2 changes)

- [Add support for job name prefix in full-pipeline](https://gitlab.com/components/opentofu/-/commit/72675b768dc3ddf81b0d665e0b5eadbebe10388e) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/153))
- [Support specifying image digests](https://gitlab.com/components/opentofu/-/commit/e6460418e6f1846f16474f6cadab2b935be556e7) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/152))

## 0.35.0-rc1 (2024-10-30)

### Fixed (1 change)

- [Only sign images on GitLab.com](https://gitlab.com/components/opentofu/-/commit/bafd24de55b17039204e9dbf1a6c9cdbb718a009) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/155))

### feature (2 changes)

- [Add support for job name prefix in full-pipeline](https://gitlab.com/components/opentofu/-/commit/72675b768dc3ddf81b0d665e0b5eadbebe10388e) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/153))
- [Support specifying image digests](https://gitlab.com/components/opentofu/-/commit/e6460418e6f1846f16474f6cadab2b935be556e7) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/152))

## 0.34.0-rc1 (2024-10-29)

No changes.

## 0.33.0 (2024-10-28)

### feature (2 changes)

- [Support GitLab dependency proxy when building gitlab-tofu images](https://gitlab.com/components/opentofu/-/commit/6bd8a73fa6c0445ec2cd44ad5bbd715afdd619a0) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/147))
- [Check for dependencies when running gitlab-tofu](https://gitlab.com/components/opentofu/-/commit/67acb93ba910d879884144e4dc27d2ff2b432dea) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/146))

## 0.32.0 (2024-10-28)

No changes.

## 0.31.0 (2024-10-24)

### Changed (1 change)

- [Improve documentation around module release](https://gitlab.com/components/opentofu/-/commit/1deda55c7f4a12b69b439af0f6d2eec4e9b97783) ([merge request](https://gitlab.com/components/opentofu/-/merge_requests/139))

## 0.30.0 (2024-10-07)

No changes.

## 0.29.0 (2024-09-20)

No changes.

## 0.28.0 (2024-09-19)

No changes.

## 0.27.0 (2024-09-05)

No changes.

## 0.26.0 (2024-08-20)

No changes.

## 0.25.0 (2024-07-30)

No changes.

## 0.24.0-rc1 (2024-07-08)

No changes.

## 0.23.0 (2024-07-05)

No changes.

## 0.22.0-rc4 (2024-07-05)

No changes.

## 0.22.0-rc3 (2024-06-19)

No changes.

## 0.22.0-rc2 (2024-06-03)

No changes.

## 0.22.0-rc1 (2024-05-31)

No changes.

## 0.21.0 (2024-05-31)

### Other (1 change)

- [Migrate unit tests from jobs to bats](components/opentofu@eed2fd8e270fbde4e21dcdeee9c0aea0545a437c) ([merge request](components/opentofu!76))

## 0.20.0 (2024-05-27)

### Added (1 change)

- [Auto URL-encode state name](components/opentofu@43148c5485b5b83cc858abb39f2033f579d2030c) ([merge request](components/opentofu!75))

## 0.19.0-rc1 (2024-05-15)

### Added (1 change)

- [Document best practice for lockfile handling](components/opentofu@e317401e6b3d8cb1b3b7f8f341eb6d3ab046961a) ([merge request](components/opentofu!64))

### Fixed (1 change)

- [Mock CI_SERVER_HOST predefined env variable for unit test](components/opentofu@1e5b1e3c269d4a38c986bdfec8dbd69456de79c2) ([merge request](components/opentofu!65))

## 0.18.0-rc5 (2024-04-08)

No changes.

## 0.18.0-rc4 (2024-04-08)

No changes.

## 0.18.0-rc3 (2024-04-08)

No changes.

## 0.18.0-rc2 (2024-04-08)

No changes.
